package com.challenge.chess;

import com.challenge.chess.piece.*;

import java.util.ArrayList;
import java.util.Arrays;

public class Board implements Cloneable  {
    private ArrayList<Piece> whiteTeam = new ArrayList<>(16);
    private ArrayList<Piece> blackTeam = new ArrayList<>(16);
    private String undo;

    /* Board map
    // [56][57][58][59][60][61][62][63]
    // [48][49][50][51][52][53][54][55]
    // [40][41][42][43][44][45][46][47]
    // [32][33][34][35][36][37][38][39]
    // [24][25][26][27][27][29][30][31]
    // [16][17][18][19][20][21][22][23]
    // [08][09][10][11][12][13][14][15]
    // [00][01][02][03][04][05][06][07]

    // [a8][b8][c8][d8][e8][f8][g8][h8]
    // [a7][b7][c7][d7][e7][f7][g7][h7]
    // [a6][b6][c6][d6][e6][f6][g6][h6]
    // [a5][b5][c5][d5][e5][f5][g5][h5]
    // [a4][b4][c4][d4][e4][f4][g4][h4]
    // [a3][b3][c3][d3][e3][f3][g3][h3]
    // [a2][b2][c2][d2][e2][f2][g2][h2]
    // [a1][b1][c1][d1][e1][f1][g1][h1]
     */
    private Piece[] board = new Piece[64];

    public static int addressToPosition(String address) {
        if (address == null || address.length() > 2)
            return -1;

        int col = "abcdefgh".indexOf(address.toLowerCase().charAt(0));
        int row = "12345678".indexOf(address.toLowerCase().charAt(1));
        if (col == -1 || row == -1)
            return -1;

        return row * 8 + col;
    }

    public King getWhiteKing() {
        return (King) whiteTeam.get(0);
    }

    public King getBlackKing() {
        return (King) blackTeam.get(0);
    }

    public Piece getPiece(int position) {
        return board[position];
    }

    private void setPiece(int position, Piece piece) {
        board[position] = piece;
        if (piece != null)
            piece.setPosition(position);
    }

    void start() {
        undo = save();
    }

    void commit() {
        undo = null;
    }

    void rollback() {
        load(undo);
    }

    boolean move(Player player, String moveFrom, String moveTo, boolean take, char newPiece) {
        int fromIdx = addressToPosition(moveFrom);
        int toIdx = addressToPosition(moveTo);
        if (toIdx < 0 || fromIdx < 0)
            return false;

        Piece p = getPiece(fromIdx);
        if (p == null || p.getPlayer() != player)
            return false;

        if (!p.canMoveTo(this, toIdx)) {
            if (!p.canAttackTo(this, toIdx))
                return false;
        }

        Piece tp = getPiece(toIdx);
        if (tp != null && tp.getPlayer() == player)
            return false;

        if (tp != null && !take)
            return false;

        if (tp instanceof King)
            return false;

        // Exchange Pawn to other piece when reach end of board
        if (p instanceof Pawn && (toIdx < 8 || toIdx > 55)) {
            if (newPiece == '\0')
                return false;

            Piece np = PieceFactory.from(newPiece);
            if (np == null)
                return false;

            removePiece(fromIdx);
            removePiece(toIdx);

            np.setPlayer(p.getPlayer());
            getTeamList(np.getPlayer()).add(np);
            setPiece(toIdx, np);
            return true;
        }

        removePiece(toIdx);
        p.move(toIdx);
        setPiece(fromIdx, null);
        setPiece(toIdx, p);
        return true;
    }

    private void move(Piece p, int from, int to) {
        p.move(to);
        setPiece(from, null);
        setPiece(to, p);
    }

    private void castleMove(int fromKing, int toKing, int fromRook, int toRook) {
        Piece k = getPiece(fromKing);
        move(k, fromKing, toKing);

        Piece r = getPiece(fromRook);
        move(r, fromRook, toRook);
    }

    boolean shortCastleMove(Player player) {
        if (player == Player.WHITE) {
            castleMove(addressToPosition("e1"), addressToPosition("g1"),
                    addressToPosition("h1"), addressToPosition("f1"));
        } else {
            castleMove(addressToPosition("e8"), addressToPosition("g8"),
                    addressToPosition("h8"), addressToPosition("f8"));
        }
        return true;
    }

    boolean longCastleMove(Player player) {
        if (player == Player.WHITE) {
            castleMove(addressToPosition("e1"), addressToPosition("c1"),
                    addressToPosition("a1"), addressToPosition("d1"));
        } else {
            castleMove(addressToPosition("e8"), addressToPosition("c8"),
                    addressToPosition("a8"), addressToPosition("d8"));
        }
        return true;
    }

    public Piece removePiece(int position) {
        if (position < 0)
            return null;

        Piece piece = getPiece(position);
        if (piece == null)
            return null;

        // King can't be removed
        if (piece instanceof King)
            return null;

        ArrayList<Piece> list = getTeamList(piece.getPlayer());
        int index = list.indexOf(piece);
        list.remove(index);

        setPiece(piece.getPosition(), null);
        return piece;
    }

    public ArrayList<Piece> getTeamList(Player player) {
        return player == Player.WHITE ? whiteTeam : blackTeam;
    }

    public ArrayList<Piece> getOppositeTeamList(Player player) {
        return player == Player.WHITE ? blackTeam : whiteTeam;
    }

    public void print() {
        System.out.println("  +---+---+---+---+---+---+---+---+");
        for (int row = 7; row >= 0; row--) {
            System.out.print(String.format("%d ", row + 1));
            for (int col = 0; col < 8; col++) {
                System.out.print(String.format("| %c ", board[row * 8 + col] == null ? ' ' : board[row * 8 + col].getSymbol()));
            }
            System.out.println("|");
            System.out.println("  +---+---+---+---+---+---+---+---+");
        }
        System.out.println("    a   b   c   d   e   f   g   h");
    }

    public boolean load(String data) {
        char[] spaces = data.toCharArray();
        if (spaces.length != 64 * 2)
            return false;

        board = new Piece[64];
        whiteTeam = new ArrayList<>(16);
        blackTeam = new ArrayList<>(16);

        for (int i = 0; i < 64; i++) {
            Piece p = PieceFactory.from(spaces[i * 2]);
            if (p != null) {
                p.setMakeMove(spaces[i * 2 + 1] == 'T');
            }
            setPiece(i, p);

            if (p == null)
                continue;

            if (p.getPlayer() == Player.WHITE) {
                if (p instanceof King) {
                    whiteTeam.add(0, p);
                } else {
                    whiteTeam.add(p);
                }
            } else {
                if (p instanceof King) {
                    blackTeam.add(0, p);
                } else {
                    blackTeam.add(p);
                }
            }
        }
        return true;
    }

    public String save() {
        char[] spaces = new char[64 * 2];
        Arrays.fill(spaces, 'x');
        for (int i = 0; i < 64; i++) {
            Piece p = getPiece(i);
            if (p != null) {
                spaces[i * 2] = p.getSymbol();
                spaces[i * 2 + 1] = p.isMakeMove() ? 'T' : 'F';
            }
        }
        return String.copyValueOf(spaces);
    }

    @Override
    public Board clone() {
        Board board = new Board();
        board.load(save());
        return board;
    }
}
