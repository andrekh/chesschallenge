package com.challenge.chess.commands;

import com.challenge.chess.Game;

import java.util.Scanner;

public class HelpCommand extends SystemCommand {

    @Override
    public String getHelpText() {
        return "this screen";
    }

    @Override
    public String getCommandTemplate() {
        return "help";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.println("Available commands:");
        for (Command cmd : CommandFactory.getCommands()) {
            System.out.println(String.format("%s - %s", cmd.getCommandTemplate(), cmd.getHelpText()));
        }

        System.out.print("Press <Enter> to return...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        return true;
    }
}
