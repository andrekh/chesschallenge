package com.challenge.chess.commands;

public class CommandFactory {
    private static Command[] commands = new Command[]{
            new ExitCommand(),
            new HelpCommand(),
            new SaveCommand(),
            new LoadCommand(),
            new DrawCommand(),
            new ResignCommand(),
            new ShortCastleCommand(),
            new LongCastleCommand(),
            new AboutCommand(),
            new MoveCommand() // must be always last command
    };

    public static Command[] getCommands() {
        return commands;
    }

    public static Command getCommand(String commandText) {
        for (Command cmd : commands) {
            if (cmd.isSupported(commandText))
                return cmd;
        }
        return null;
    }
}
