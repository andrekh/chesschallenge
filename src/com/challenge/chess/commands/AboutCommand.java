package com.challenge.chess.commands;

import com.challenge.chess.Game;

import java.util.Scanner;

public class AboutCommand extends SystemCommand {

    @Override
    public String getHelpText() {
        return "about game";
    }

    @Override
    public String getCommandTemplate() {
        return "about";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.println("Chess Challenge\nby Andrii Khoruzhenko");
        System.out.println("andrekh@gmail.com");
        System.out.println("December 7, 2019");
        System.out.print("Press <Enter> to return...");
        Scanner scanner = new Scanner(System.in);
        scanner.nextLine();
        return true;
    }
}
