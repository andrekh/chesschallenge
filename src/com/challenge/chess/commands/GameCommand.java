package com.challenge.chess.commands;

import com.challenge.chess.Game;
import com.challenge.chess.rules.Rule;

import java.util.ArrayList;
import java.util.List;

public abstract class GameCommand implements Command {

    public abstract String getCommandTemplate();

    public abstract boolean go(Game game, String commandText);

    protected List<Rule> getPreRules() {
        return new ArrayList<Rule>();
    }

    protected List<Rule> getPostRules() {
        return new ArrayList<Rule>();
    }

    @Override
    public String getHelpText() {
        return null;
    }

    @Override
    public boolean isSupported(String commandText) {
        if (getCommandTemplate() == null || commandText == null)
            return false;

        return getCommandTemplate().equalsIgnoreCase(commandText.trim());
    }

    public boolean execute(Game game, String commandText) {
        game.start();
        try {

            for (Rule rule : getPreRules()) {
                if (!rule.isValid(game)) {
                    System.err.println(rule.getErrorMessage());
                    System.out.println("");
                    game.rollback();
                    return false;
                }
            }

            if (!go(game, commandText)) {
                System.err.println(String.format("Invalid move: %s", commandText));
                System.out.println("");
                game.rollback();
                return false;
            }

            for (Rule rule : getPostRules()) {
                if (!rule.isValid(game)) {
                    System.err.println(rule.getErrorMessage());
                    System.out.println("");
                    game.rollback();
                    return false;
                }
            }
            game.commit();
            return true;
        } catch (Exception e) {
            game.rollback();
            return false;
        }
    }
}
