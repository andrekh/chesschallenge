package com.challenge.chess.commands;

import com.challenge.chess.Game;

public abstract class SystemCommand implements Command {

    public abstract String getCommandTemplate();

    public abstract boolean go(Game game, String commandText);

    @Override
    public String getHelpText() {
        return null;
    }

    @Override
    public boolean isSupported(String commandText) {
        if (getCommandTemplate() == null || commandText == null)
            return false;

        return getCommandTemplate().equalsIgnoreCase(commandText.trim());
    }

    public boolean execute(Game game, String commandText) {
        if (!go(game, commandText)) {
            System.err.println(String.format("Error: %s", commandText));
            System.out.println("");
            return false;
        }
        return true;
    }
}
