package com.challenge.chess.commands;

import com.challenge.chess.Game;
import com.challenge.chess.rules.KingLongCastleRule;
import com.challenge.chess.rules.Rule;

import java.util.List;

public class LongCastleCommand extends GameCommand {
    @Override
    public String getCommandTemplate() {
        return "o-o-o";
    }

    @Override
    public String getHelpText() {
        return "long сastling";
    }

    @Override
    protected List<Rule> getPreRules() {
        List<Rule> rules = super.getPreRules();
        rules.add(new KingLongCastleRule());
        return rules;
    }

    @Override
    public boolean go(Game game, String commandText) {
        return game.longCastleMove();
    }
}
