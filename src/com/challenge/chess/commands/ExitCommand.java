package com.challenge.chess.commands;

import com.challenge.chess.Game;

public class ExitCommand extends SystemCommand {

    @Override
    public String getHelpText() {
        return "leave game";
    }

    @Override
    public String getCommandTemplate() {
        return "exit";
    }

    @Override
    public boolean go(Game game, String commandText) {
        game.exit();
        return true;
    }
}
