package com.challenge.chess.commands;

import com.challenge.chess.Game;
import com.challenge.chess.Player;

import java.util.Scanner;

public class DrawCommand extends GameCommand {
    @Override
    public String getHelpText() {
        return "ask to draw game";
    }

    @Override
    public String getCommandTemplate() {
        return "draw";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.printf("%s # Are you agree (yes/No)? ",
                (game.getCurrentPlayer() == Player.WHITE ? Player.BLACK : Player.WHITE));

        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        if ("yes".equalsIgnoreCase(str.trim())) {
            game.draw();
        }

        return true;
    }
}
