package com.challenge.chess.commands;

import com.challenge.chess.Game;

import java.util.Scanner;

public class LoadCommand extends SystemCommand {

    @Override
    public String getHelpText() {
        return "load game from input";
    }

    @Override
    public String getCommandTemplate() {
        return "load";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.print("GAME DATA # ");

        Scanner scanner = new Scanner(System.in);
        String data = scanner.nextLine();

        if (!game.load(data))
            System.err.println("Error loading game!!!");

        System.out.println("Game loaded");
        return true;
    }
}
