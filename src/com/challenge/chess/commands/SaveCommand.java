package com.challenge.chess.commands;

import com.challenge.chess.Game;

public class SaveCommand extends SystemCommand {

    @Override
    public String getHelpText() {
        return "save game to screen";
    }

    @Override
    public String getCommandTemplate() {
        return "save";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.print("GAME DATA # ");
        System.out.println(game.save());
        return true;
    }
}
