package com.challenge.chess.commands;

import com.challenge.chess.Game;
import com.challenge.chess.rules.KingCheckRule;
import com.challenge.chess.rules.Rule;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoveCommand extends GameCommand {

    @Override
    public String getHelpText() {
        return  "regular move where:\n" +
                "      ss - start move (e2)\n" +
                "      x  - (optional) to take opponent piece\n" +
                "      ee - end move (e4)\n" +
                "      p  - (optional) target piece when pawn reach last line";
    }

    @Override
    public String getCommandTemplate() {
        return "ss[x]ee[p]";
    }

    @Override
    public boolean isSupported(String commandText) {
        Matcher m = parseCommandText(commandText);
        return m.find();
    }

    private static Matcher parseCommandText(String commandText) {
        Pattern pattern = Pattern.compile("([a-h])([1-8])(x?)([a-h])([1-8])([rnbq]?)",
                Pattern.CASE_INSENSITIVE);

        return pattern.matcher(commandText);
    }

    @Override
    protected List<Rule> getPostRules() {
        List<Rule> rules = super.getPostRules();
        rules.add(new KingCheckRule());
        return rules;
    }

    @Override
    public boolean go(Game game, String commandText) {
        if (commandText == null)
            return false;

        Matcher m = parseCommandText(commandText.toLowerCase());
        if (!m.find())
            return false;

        String moveFrom = m.group(1) + m.group(2);
        String moveTo = m.group(4) + m.group(5);
        boolean take = "x".equalsIgnoreCase(m.group(3));
        char newPiece = m.group(6).length() > 0 ? m.group(6).charAt(0) : '\0';

        return game.move(moveFrom, moveTo, take, newPiece);
    }
}
