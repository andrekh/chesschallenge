package com.challenge.chess.commands;

import com.challenge.chess.Game;

public interface Command {
    boolean isSupported(String commandText);
    String getHelpText();
    String getCommandTemplate();
    boolean execute(Game game, String commandText);
}
