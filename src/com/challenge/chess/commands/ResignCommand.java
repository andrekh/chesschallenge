package com.challenge.chess.commands;

import com.challenge.chess.Game;

import java.util.Scanner;

public class ResignCommand extends GameCommand {

    @Override
    public String getHelpText() {
        return "resign game";
    }

    @Override
    public String getCommandTemplate() {
        return "resign";
    }

    @Override
    public boolean go(Game game, String commandText) {
        System.out.printf("%s # Are you sure (yes/No)? ", game.getCurrentPlayer());
        Scanner scanner = new Scanner(System.in);
        String str = scanner.nextLine();
        if ("yes".equalsIgnoreCase(str.trim())) {
            game.resign(game.getCurrentPlayer());
        }

        return true;
    }
}
