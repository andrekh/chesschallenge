package com.challenge.chess.commands;

import com.challenge.chess.Game;
import com.challenge.chess.rules.KingShortCastleRule;
import com.challenge.chess.rules.Rule;

import java.util.List;

public class ShortCastleCommand extends GameCommand {
    @Override
    public String getCommandTemplate() {
        return "o-o";
    }

    @Override
    public String getHelpText() {
        return "short сastling";
    }

    @Override
    protected List<Rule> getPreRules() {
        List<Rule> rules = super.getPreRules();
        rules.add(new KingShortCastleRule());
        return rules;
    }

    @Override
    public boolean go(Game game, String commandText) {
        return game.shortCastleMove();
    }
}
