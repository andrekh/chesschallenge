package com.challenge.chess.rules;

import com.challenge.chess.Game;

public interface Rule {
    boolean isValid(Game game);
    String getErrorMessage();
}
