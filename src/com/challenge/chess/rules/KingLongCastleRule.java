package com.challenge.chess.rules;

import com.challenge.chess.Board;
import com.challenge.chess.Player;

public class KingLongCastleRule extends KingCastleRule {

    @Override
    protected int[] getPositions(Board board, Player player) {
        if (player == Player.WHITE) {
            return new int[] {
                    Board.addressToPosition("a1"),
                    Board.addressToPosition("b1"),
                    Board.addressToPosition("c1"),
                    Board.addressToPosition("d1"),
                    Board.addressToPosition("e1")
            };
        } else {
            return new int[] {
                    Board.addressToPosition("a8"),
                    Board.addressToPosition("b8"),
                    Board.addressToPosition("c8"),
                    Board.addressToPosition("d8"),
                    Board.addressToPosition("e8")
            };
        }
    }
}
