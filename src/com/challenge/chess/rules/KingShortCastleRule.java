package com.challenge.chess.rules;

import com.challenge.chess.Board;
import com.challenge.chess.Player;

public class KingShortCastleRule extends KingCastleRule {

    @Override
    protected int[] getPositions(Board board, Player player) {
        if (player == Player.WHITE) {
            return new int[] {
                    Board.addressToPosition("h1"),
                    Board.addressToPosition("g1"),
                    Board.addressToPosition("f1"),
                    Board.addressToPosition("e1")
            };
        } else {
            return new int[] {
                    Board.addressToPosition("h8"),
                    Board.addressToPosition("g8"),
                    Board.addressToPosition("f8"),
                    Board.addressToPosition("e8")
            };
        }
    }
}
