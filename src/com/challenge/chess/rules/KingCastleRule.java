package com.challenge.chess.rules;

import com.challenge.chess.Board;
import com.challenge.chess.Game;
import com.challenge.chess.Player;
import com.challenge.chess.piece.King;
import com.challenge.chess.piece.Piece;
import com.challenge.chess.piece.Rook;

import java.util.ArrayList;

public abstract class KingCastleRule implements Rule {

    // Rook position must be first in a result array
    protected abstract int[] getPositions(Board board, Player player);

    @Override
    public boolean isValid(Game game) {
        if (game == null)
            return false;

        Board board = game.getBoard();
        King king = game.getCurrentPlayer() == Player.WHITE ? board.getWhiteKing() : board.getBlackKing();

        if (king.isMakeMove())
            return false;

        int[] positions = getPositions(board, king.getPlayer());

        Piece rook = board.getPiece(positions[0]);
        if (rook == null || rook.isMakeMove() || rook.getPlayer() != king.getPlayer()|| !(rook instanceof Rook))
            return false;

        for (int i : positions) {
            Piece p = board.getPiece(i);

            if (p != null && p != king && p != rook)
                return false;
        }

        ArrayList<Piece> team = board.getOppositeTeamList(king.getPlayer());
        for (int i = 0; i < team.size(); i++) {
            Piece p = team.get(i);
            for (int pos : positions) {
                if (p.canAttackTo(board, pos))
                    return false;
            }
        }
        return true;
    }

    @Override
    public String getErrorMessage() {
        return "You can't do castle move";
    }
}
