package com.challenge.chess.rules;

import com.challenge.chess.Board;
import com.challenge.chess.Game;
import com.challenge.chess.Player;
import com.challenge.chess.piece.King;
import com.challenge.chess.piece.Piece;

import java.util.ArrayList;

public class KingCheckRule implements Rule {

    @Override
    public boolean isValid(Game game) {
        Board board = game.getBoard();
        King king = game.getCurrentPlayer() == Player.WHITE ? board.getWhiteKing() : board.getBlackKing();

        ArrayList<Piece> team = board.getOppositeTeamList(king.getPlayer());
        for (int i = 0; i < team.size(); i++) {
            Piece p = team.get(i);
            if (p.canAttackTo(board, king.getPosition()))
                return false;
        }
        return true;
    }

    @Override
    public String getErrorMessage() {
        return "CHECK";
    }
}
