package com.challenge.chess.rules;

import com.challenge.chess.Board;
import com.challenge.chess.Game;
import com.challenge.chess.Player;
import com.challenge.chess.piece.King;
import com.challenge.chess.piece.Knight;
import com.challenge.chess.piece.Piece;

import java.util.ArrayList;
import java.util.List;

public class KingСheckmateRule implements Rule {

    @Override
    public boolean isValid(Game game) {
        Board board = game.getBoard();
        King king = game.getCurrentPlayer() == Player.WHITE ? board.getWhiteKing() : board.getBlackKing();

        // 1. King under attack
        ArrayList<Piece> oppositeTeam = board.getOppositeTeamList(king.getPlayer());
        List<Piece> attackers = new ArrayList<>();
        for (int i = 0; i < oppositeTeam.size(); i++) {
            Piece p = oppositeTeam.get(i);
            if (p.canAttackTo(board, king.getPosition())) {
                attackers.add(p);
            }
        }

        if (attackers.size() == 0) return true;

        // 2. King can't move
        List<Integer> kingMoves = king.getMoves(board);
        if (kingMoves.size() > 0) return true;

        // 3. if 2 attackers then checkmate
        if (attackers.size() > 1) return false;

        // 4. Can kill attacker
        ArrayList<Piece> playerTeam = board.getTeamList(king.getPlayer());
        Piece attacker = attackers.get(0); // we sure have only one here
        for (int i = 1; i < playerTeam.size(); i++) { // skip King here
            Piece p = playerTeam.get(i);
            if (p.canAttackTo(board, attacker.getPosition())) { // we can kill attacker
                return true;
            }
        }

        // 5. Is attacker knight? if yes then checkmate
        if (attacker instanceof Knight)
            return false;

        // 6. is attacker next to king?
        int kingRow = king.getPosition() / 8;
        int kingCol = king.getPosition() % 8;
        int attackerRow = attacker.getPosition() / 8;
        int attackerCol = attacker.getPosition() % 8;
        if (Math.abs(attackerCol - kingCol) == 1 || Math.abs(attackerRow - kingRow) == 1)
            return false;

        // 7. can we cover king
        if (kingRow == attackerRow) { // on same row check all spaces between
            int start = Math.min(kingCol, attackerCol) + 1;
            int stop = Math.max(kingCol, attackerCol);
            for (int col = start; col < stop; col++) {
                for (int i = 1; i < playerTeam.size(); i++) { // skip King here
                    Piece p = playerTeam.get(i);
                    if (p.canMoveTo(board, kingRow * 8 + col)) {
                        return true;
                    }
                }
            }
        } else if (kingCol == attackerCol) {
            int start = Math.min(kingRow, attackerRow) + 1;
            int stop = Math.max(kingRow, attackerRow);
            for (int row = start; row < stop; row++) {
                for (int i = 1; i < playerTeam.size(); i++) { // skip King here
                    Piece p = playerTeam.get(i);
                    if (p.canMoveTo(board, row * 8 + kingCol)) {
                        return true;
                    }
                }
            }
        } else {
            int startRow = Math.min(kingRow, attackerRow) + 1;
            int col = Math.min(kingCol, attackerCol) + 1;
            int stopRow = Math.max(kingRow, attackerRow);
            for (int row = startRow; row < stopRow; row++, col++) {
                for (int i = 1; i < playerTeam.size(); i++) { // skip King here
                    Piece p = playerTeam.get(i);
                    if (p.canMoveTo(board, row * 8 + col)) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    @Override
    public String getErrorMessage() {
        return "CHECKMATE";
    }
}
