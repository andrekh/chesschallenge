package com.challenge.chess.piece;

import com.challenge.chess.Board;
import com.challenge.chess.Player;

import java.util.ArrayList;
import java.util.List;

public class King extends Piece {

    public King(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        return new int[][] {
                {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}
        };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'K' : 'k';
    }

    @Override
    protected boolean canReach(Board board, int target, int[][] offsets, boolean ignoreTarget) {
        boolean possibleMove = super.canReach(board, target, offsets, ignoreTarget);
        if (!possibleMove)
            return false;

        ArrayList<Piece> team = board.getOppositeTeamList(getPlayer());
        for (int i = 0; i < team.size(); i++) {
            Piece p = team.get(i);
            if (p.canAttackTo(board, target))
                return false;
        }

        return true;
    }

    @Override
    public List<Integer> getMoves(Board board) {
        List<Integer> moves = super.getMoves(board);
        if (moves.size() == 0)
            return moves;

        ArrayList<Piece> team = board.getOppositeTeamList(getPlayer());
        int idx = 0;
        while (idx < moves.size()) {
            boolean removed = false;
            for (Piece p : team) {
                if (p.canProtectAt(board, moves.get(idx))) {
                    moves.remove(idx);
                    removed = true;
                    break;
                }
            }
            if (!removed)
                idx++;
        }
        return moves;
    }
}
