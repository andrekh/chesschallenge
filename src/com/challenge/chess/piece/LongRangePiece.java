package com.challenge.chess.piece;

import com.challenge.chess.Board;

public abstract class LongRangePiece extends Piece {

    @Override
    protected boolean canReach(Board board, int target, int[][] offsets, boolean ignoreTarget) {
        if (target == getPosition()) return false;

        if (!ignoreTarget) {
            Piece targetPiece = board.getPiece(target);
            if (targetPiece != null && targetPiece.getPlayer() == getPlayer())
                return false;
        }

        int targetRow = target / 8;
        int targetCol = target % 8;

        for (int i = 0; i < offsets.length; i++) {
            int currentCol = getPosition() % 8 + offsets[i][0];
            int currentRow = getPosition() / 8 + offsets[i][1];
            while (currentCol < 8 && currentCol >= 0 && currentRow < 8 && currentRow >= 0) {
                if (targetRow == currentRow && targetCol == currentCol)
                    return true;

                int currentPosition = currentRow * 8 + currentCol;

                //if (ignoreTarget)
                Piece p = board.getPiece(currentPosition);
                if (ignoreTarget && p instanceof King) {
                    currentCol += offsets[i][0];
                    currentRow += offsets[i][1];
                    continue;
                }

                if (p != null)
                    break;

                currentCol += offsets[i][0];
                currentRow += offsets[i][1];

            }
        }
        return false;
    }
}
