package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class Pawn extends Piece {

    public Pawn(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        int flag = (getPlayer() == Player.WHITE ? 1 : -1);
        if (isMakeMove())
            return new int[][] { {0, flag} };

        return new int[][] { {0, flag}, {0, 2 * flag} };
    }

    @Override
    protected int[][] attackOffsets() {
        int flag = (getPlayer() == Player.WHITE ? 1 : -1);
        return new int[][] { {-1, flag}, {1, flag} };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'P' : 'p';
    }
}
