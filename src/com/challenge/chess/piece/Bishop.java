package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class Bishop extends LongRangePiece {

    public Bishop(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        return new int[][] { {1, 1}, {1, -1}, {-1, -1}, {-1, 1} };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'B' : 'b';
    }
}
