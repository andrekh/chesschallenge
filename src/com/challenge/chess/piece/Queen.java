package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class Queen extends LongRangePiece {

    public Queen(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        return new int[][] {
                {0, 1}, {1, 1}, {1, 0}, {1, -1}, {0, -1}, {-1, -1}, {-1, 0}, {-1, 1}
        };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'Q' : 'q';
    }
}
