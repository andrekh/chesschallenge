package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class PieceFactory {
    public static Piece from(char piece) {
        switch (piece) {
            case 'P':
                return new Pawn(Player.WHITE);
            case 'p':
                return new Pawn(Player.BLACK);
            case 'R':
                return new Rook(Player.WHITE);
            case 'r':
                return new Rook(Player.BLACK);
            case 'N':
                return new Knight(Player.WHITE);
            case 'n':
                return new Knight(Player.BLACK);
            case 'B':
                return new Bishop(Player.WHITE);
            case 'b':
                return new Bishop(Player.BLACK);
            case 'Q':
                return new Queen(Player.WHITE);
            case 'q':
                return new Queen(Player.BLACK);
            case 'K':
                return new King(Player.WHITE);
            case 'k':
                return new King(Player.BLACK);
            default:
                return null;
        }
    }
}
