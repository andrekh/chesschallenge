package com.challenge.chess.piece;

import com.challenge.chess.Board;
import com.challenge.chess.Player;

import java.util.ArrayList;
import java.util.List;

public abstract class Piece {
    private Player player;
    private boolean makeMove = false;
    private int position = -1;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public Player getPlayer() {
        return player;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public boolean isMakeMove() {
        return makeMove;
    }

    public void setMakeMove(boolean makeMove) {
        this.makeMove = makeMove;
    }

    protected abstract int[][] moveOffsets();

    protected int[][] attackOffsets() {
        return moveOffsets();
    }

    public boolean move(int position) {
        setPosition(position);
        makeMove = true;
        return true;
    }

    public abstract char getSymbol();

    protected boolean canReach(Board board, int target, int[][] offsets, boolean ignoreTarget) {
        if (target == getPosition()) return false;

        if (!ignoreTarget) {
            Piece targetPiece = board.getPiece(target);
            if (targetPiece != null && targetPiece.getPlayer() == getPlayer())
                return false;
        }

        int targetRow = target / 8;
        int targetCol = target % 8;

        for (int i = 0; i < offsets.length; i++) {
            int currentCol = getPosition() % 8 + offsets[i][0];
            int currentRow = getPosition() / 8 + offsets[i][1];

            if (currentRow > 7 || currentCol > 7 || currentCol < 0 || currentRow < 0)
                continue;

            if (targetRow == currentRow && targetCol == currentCol)
                return true;
        }
        return false;
    }

    public List<Integer> getMoves(Board board) {
        return getMoves(board, moveOffsets());
    }

    protected List<Integer> getMoves(Board board, int[][] offsets) {
        List<Integer> moves = new ArrayList<>();

        for (int i = 0; i < offsets.length; i++) {
            int currentCol = getPosition() % 8 + offsets[i][0];
            int currentRow = getPosition() / 8 + offsets[i][1];

            if (currentRow > 7 || currentCol > 7 || currentCol < 0 || currentRow < 0)
                continue;

            int target = currentRow * 8 + currentCol;

            Piece piece = board.getPiece(target);
            if (piece == null || piece.getPlayer() != getPlayer())
                moves.add(target);
        }
        return moves;
    }

    public boolean canAttackTo(Board board, int target) {
        return canReach(board, target, attackOffsets(), false);
    }

    public boolean canProtectAt(Board board, int target) {
        return canReach(board, target, attackOffsets(), true);
    }

    public boolean canMoveTo(Board board, int target) {
        return canReach(board, target, moveOffsets(), false);
    }
}
