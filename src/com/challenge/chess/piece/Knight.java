package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class Knight extends Piece {

    public Knight(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        return new int[][] {
                {1, 2}, {2, 1}, {2, -1}, {1, -2}, {-1, -2}, {-2, -1}, {-2, 1}, {-1, 2}
        };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'N' : 'n';
    }
}
