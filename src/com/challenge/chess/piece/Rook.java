package com.challenge.chess.piece;

import com.challenge.chess.Player;

public class Rook extends LongRangePiece {

    public Rook(Player player) {
        super();
        setPlayer(player);
    }

    @Override
    protected int[][] moveOffsets() {
        return new int[][] { {0, 1}, {1, 0}, {0, -1}, {-1, 0} };
    }

    @Override
    public char getSymbol() {
        return getPlayer() == Player.WHITE ? 'R' : 'r';
    }
}
