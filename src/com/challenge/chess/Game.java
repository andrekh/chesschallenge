package com.challenge.chess;

public class Game {
    private static String WHITE_FIRST = "W" +
            "RFNFBFQFKFBFNFRFPFPFPFPFPFPFPFPF" +
            "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" +
            "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" +
            "pFpFpFpFpFpFpFpFrFnFbFqFkFbFnFrF";

    private Board board;
    private Player currentPlayer;
    private boolean finished;

    public Game() {
        board = new Board();
        load(WHITE_FIRST);
    }

    public void start() {
        board.start();
    }

    public void rollback() {
        board.rollback();
    }

    public void commit() {
        board.commit();
        nextTurn();
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    private void nextTurn() {
        currentPlayer = currentPlayer == Player.WHITE ? Player.BLACK : Player.WHITE;
    }

    public Board getBoard() {
        return board;
    }

    public boolean isFinished() {
        return finished;
    }

    public void draw() {
        System.out.println("Game over with result:\n\n 1/2 - 1/2");
        finished = true;
    }

    public void exit() {
        System.out.println("Game canceled");
        finished = true;
    }

    public void checkmate() {
        System.out.println(String.format("Game over with result:\n\n %s - %s",
                (getCurrentPlayer() == Player.WHITE ? "0" : "1"),
                (getCurrentPlayer() == Player.BLACK ? "0" : "1")));
        finished = true;
    }

    public void resign(Player resignPlayer) {
        System.out.println(String.format("Game over with result:\n\n %s - %s",
                (getCurrentPlayer() == Player.WHITE ? "0" : "1"),
                (getCurrentPlayer() == Player.BLACK ? "0" : "1")));
        finished = true;
    }

    public boolean shortCastleMove() {
        board.shortCastleMove(getCurrentPlayer());
        return true;
    }

    public boolean longCastleMove() {
        board.longCastleMove(getCurrentPlayer());
        return true;
    }

    public boolean move(String moveFrom, String moveTo, boolean take, char newPiece) {
        if (!board.move(getCurrentPlayer(), moveFrom, moveTo, take, newPiece))
            return false;
        return true;
    }

    public boolean load(String data) {
        if (data == null || data.length() != 64 * 2 + 1)
            return false;

        currentPlayer = data.charAt(0) == 'W' ? Player.WHITE : Player.BLACK;
        return board.load(data.substring(1));
    }

    public String save() {
        return (currentPlayer == Player.WHITE ? "W" : "B") + board.save();
    }
}
