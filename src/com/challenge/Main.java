package com.challenge;

import com.challenge.chess.Game;
import com.challenge.chess.commands.Command;
import com.challenge.chess.commands.CommandFactory;
import com.challenge.chess.rules.KingCheckRule;
import com.challenge.chess.rules.KingСheckmateRule;
import com.challenge.chess.rules.Rule;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("Welcome to CHESS challenge!!!");

        Rule checkRule = new KingCheckRule();
        Rule checkmateRule = new KingСheckmateRule();

        Game game = new Game();
        while (!game.isFinished()) {
            game.getBoard().print();

            if (!checkmateRule.isValid(game)) {
                System.out.println(checkmateRule.getErrorMessage());
                game.checkmate();
                break;
            }

            if (!checkRule.isValid(game)) {
                System.out.println(checkRule.getErrorMessage());
            }

            System.out.printf("%s your move # ", game.getCurrentPlayer().toString());

            Scanner scanner = new Scanner(System.in);
            String commandText = scanner.nextLine();

            Command command = CommandFactory.getCommand(commandText);
            if (command == null) {
                System.err.println("Unrecognized command. Try again. help to available commands");
                System.out.println("");
                continue;
            }
            command.execute(game, commandText);
        }
        System.out.println("\nGood bye...");
    }
}
