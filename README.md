# Chess Challenge by Andrii Khoruzhenko

To develop used JetBrains IntelliJ IDEA 2019.3 with Java 1.8

After start application you can start game immeditly.

Awailable commands:

exit - leave game
help - this screen
save - save game to screen
load - load game from input
draw - ask to draw game
resign - resign game
o-o - short сastling
o-o-o - long сastling
ss[x]ee[p] - regular move where:
      ss - start move (e2)
      x  - (optional) to take opponent piece
      ee - end move (e4)
      p  - (optional) target piece when pawn reach last line
Press <Enter> to continue game...

use help to show this screen.

Supported:
1. CHECK validation
2. CHECKMATE validation
3. Move validation
 
Unsupported:
1. en passant

System commands like save and load used screed for output and grabb data.

