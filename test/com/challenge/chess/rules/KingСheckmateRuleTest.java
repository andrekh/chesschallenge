package com.challenge.chess.rules;

import com.challenge.chess.Game;
import com.challenge.chess.commands.Command;
import com.challenge.chess.commands.MoveCommand;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class KingСheckmateRuleTest {

    @Test
    void kingCheckmateRuleTest() {
        String data = "BRFxxBFxxxxxxKTxxxxPFPFPFxxPFPFPFxxPTxxxxxxxxxxxxxx" +
                "RTxxxxxxxxxxxxxxxxxxxxxxxxxxqTpTxxxxxxxxxxxxxxxxxxp" +
                "FpFbTpFpFpFrFxxxxxxkFxxnFrF";
        Game game = new Game();
        game.load(data);

        Rule rule = new KingСheckmateRule();
        assertTrue(rule.isValid(game));

        Command cmd = new MoveCommand();
        assertTrue(cmd.execute(game, "h5d1"));
        assertFalse(rule.isValid(game));
    }
}