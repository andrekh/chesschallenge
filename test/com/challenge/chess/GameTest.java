package com.challenge.chess;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class GameTest {

    @Test
    void loadSaveTest() {
        String data = "WRFNFBFxxKFxxNFRFPFPFPFPFxxPFPFPFxxxxxxxxxxxxxxxx" +
                "pTxxBTxxPTxxxxxxxxxxxxxxxxxxxxQTxxxxxxxxxxxxxxxxxx" +
                "pFpFpFpFpFpFpFrFnFbFqFkFbFnFrF";
        Game game = new Game();
        game.load(data);
        data.equals(game.save());
    }
}