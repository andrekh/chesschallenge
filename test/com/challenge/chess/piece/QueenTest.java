package com.challenge.chess.piece;

import com.challenge.chess.Board;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class QueenTest {

    @Test
    void canAttack() {
        String data = "RFNFBFxxKFxxNFRFPFPFPFPFxxPFPFPFxxxxxxxxxxxxxxxx" +
                "pTxxBTxxPTxxxxxxxxxxxxxxxxxxxxQTxxxxxxxxxxxxxxxxxx" +
                "pFpFpFpFpFpFpFrFnFbFqFkFbFnFrF";

        Board board = new Board();
        board.load(data);

        Queen queen = (Queen) board.getPiece(Board.addressToPosition("h5"));
        assertNotEquals(null, queen);

        assertFalse(queen.canAttackTo(board, Board.addressToPosition("g2")));
        assertFalse(queen.canAttackTo(board, Board.addressToPosition("h8")));

        board.removePiece(Board.addressToPosition("h7"));
        assertTrue(queen.canAttackTo(board, Board.addressToPosition("h8")));
    }
}