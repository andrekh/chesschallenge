package com.challenge.chess;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BoardTest {

    @Test
    void loadSaveTest() {
        String data = "RFNFBFxxKFxxNFRFPFPFPFPFxxPFPFPFxxxxxxxxxxxxxxxx" +
                "pTxxBTxxPTxxxxxxxxxxxxxxxxxxxxQTxxxxxxxxxxxxxxxxxx" +
                "pFpFpFpFpFpFpFrFnFbFqFkFbFnFrF";
        Board board = new Board();
        assertEquals(true, board.load(data));
        assertEquals(data, board.save());
    }

    @Test
    void addressToPositionTest() {
        assertEquals(0, Board.addressToPosition("a1"));
        assertEquals(63, Board.addressToPosition("h8"));
        assertEquals(-1, Board.addressToPosition("a9"));
        assertEquals(-1, Board.addressToPosition("j8"));
    }
}