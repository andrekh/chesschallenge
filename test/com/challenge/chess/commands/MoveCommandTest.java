package com.challenge.chess.commands;

import com.challenge.chess.Game;

import static org.junit.jupiter.api.Assertions.*;

class MoveCommandTest {

    @org.junit.jupiter.api.Test
    void moveCommandTests() {
        String data = "WRFNFBFxxKFxxNFRFPFPFPFPFxxPFPFPFxxxxxxxxxxxxxxxx" +
                "pTxxBTxxPTxxxxxxxxxxxxxxxxxxxxQTxxxxxxxxxxxxxxxxxx" +
                "pFpFpFpFpFpFpFrFnFbFqFkFbFnFrF";
        Game game = new Game();
        game.load(data);

        Command cmd = new MoveCommand();
        assertFalse(cmd.execute(game, "e2e4"));
        assertFalse(cmd.execute(game, "e2j4"));
        assertTrue(cmd.execute(game, "h5xf7"));
    }
}